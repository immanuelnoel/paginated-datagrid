#PAGINATED DATAGRID     
##Actionscript component    
      
This component extends dataGrid, displaying only a set of records at a time, thereby organizing the entire collection into pages.        
        
-> Arranges the collection specified under the dataProvider attribute into pages.     
-> Displays a footer at the bottom Indicates currently displayed records 2 navigation buttons (Previous, Next)        
       
Read more at,      
http://blog.immanuelnoel.com/2010/03/06/flex-paginated-datagrid/          